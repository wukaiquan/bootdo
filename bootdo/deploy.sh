#!/usr/bin/env bash
#编译+部署

# 需要配置如下参数
# 项目路径，在Execute Shell中配置项目路径，pwd 或许该项目路径
# export PROJ_PATH = 这个jekins任务在部署机器上的路径

# 输入你的环境上的tomcat的全路径
# export TOMCAT_APP_PATH = tomcat 在部署机器上的路径

### base 函数"
killTomcat(){
  pid= `ps -ef|grep tomcat|grep java|awk '{print $2}'`
  echo "tomcat id list:$pid"
  if [ "$pid" = "" ]
  then
    echo "no tomcat pid alive"
  else
    kill -9 $pid
  fi
}

cd $PROJ_PATH/bootdo
mvn clean install

# 停tomcat
killTomcat