package com.bootdo.testDemo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * @Author wukq
 * @Date: 2020/4/16 19:35
 * @Description:
 */
public class TestJson {
    //json字符串-简单对象型
    private static final String JSON_OBJ_STR = "{\"studentName\":\"lily\",\"studentAge\":12}";
    //json字符串-数组类型
    private static final String JSON_ARRAY_STR = "[{\"studentName\":\"lily\",\"studentAge\":12},{\"studentName\":\"lucy\",\"studentAge\":15}]";

    private static final String JSON_ARRAY_STR1 = "[{\"buildingFloorId\":\"37\",\"floor\":\"18\",\"roomNumber\":\"101\"},{\"buildingFloorId\":\"37\",\"floor\":\"19\",\"roomNumber\":\"102\"}]";
    private static final String JSON_ARRAY_STR2 = "[{buildingFloorId=37, roomNumber=101, floor=18}, {buildingFloorId=37, roomNumber=102, floor=19}]";
    private static final String JSON_ARRAY_STR3 = "{createTime=2020-04-16 12:34:11, phone=123456798}";
    //{"createTime": "2020 - 04 - 16 12: 34: 11","phone": "123456798"} 妈蛋，这样的才算是正确的JSON
    //复杂格式json字符串
    private static final String COMPLEX_JSON_STR = "{\"teacherName\":\"crystall\",\"teacherAge\":27,\"course\":{\"courseName\":\"english\",\"code\":1270},\"students\":[{\"studentName\":\"lily\",\"studentAge\":12},{\"studentName\":\"lucy\",\"studentAge\":15}]}";

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        //demoJson();

        //testJSONStrToJSONObject();//json字符串转化对象
        //testJSONStrToJSONArray();//json数组转化json对象
        //testComplexJSONStrToJSONObject();//json对象嵌套json对象
        testJSONStrToJSONArray2();
    }

    /**
     * 复杂json格式字符串与JSONObject之间的转换
     */
    public static void testComplexJSONStrToJSONObject() {
        System.out.println(COMPLEX_JSON_STR);
        JSONObject jsonObject = JSON.parseObject(COMPLEX_JSON_STR);
        //JSONObject jsonObject1 = JSONObject.parseObject(COMPLEX_JSON_STR);//因为JSONObject继承了JSON，所以这样也是可以的
        System.out.println(jsonObject);
        String teacherName = jsonObject.getString("teacherName");
        Integer teacherAge = jsonObject.getInteger("teacherAge");
        JSONObject course = jsonObject.getJSONObject("course");
        JSONArray students = jsonObject.getJSONArray("students");
        System.out.println(teacherName + "------" + teacherAge + "===json对象====" + course + "----json数组----" + students);
        JSONArray jsonArray = JSON.parseArray(students.toString());
        System.out.println(jsonArray);
    }

    /**
     * json字符串-数组类型与JSONArray之间的转换
     */
    public static void testJSONStrToJSONArray() {

        JSONArray jsonArray = JSON.parseArray(JSON_ARRAY_STR1);
        //JSONArray jsonArray1 = JSONArray.parseArray(JSON_ARRAY_STR);//因为JSONArray继承了JSON，所以这样也是可以的

        //遍历方式1
        int size = jsonArray.size();
        for (int i = 0; i < size; i++) {
//            JSONObject jsonObject = jsonArray.getJSONObject(i);
//            System.out.println(jsonObject.getString("studentName") + ":" + jsonObject.getInteger("studentAge"));
            Map<String, Object> map = (Map<String, Object>) jsonArray.get(i);
            System.out.println(map.get("buildingFloorId") + ":" + map.get("floor"));
        }


        //遍历方式2
        for (Object obj : jsonArray) {
//            JSONObject jsonObject = (JSONObject) obj;
//            System.out.println(jsonObject.getString("studentName") + ":" + jsonObject.getInteger("studentAge"));

            Map<String, Object> map = (Map<String, Object>) obj;
            System.out.println(map.get("buildingFloorId") + ":" + map.get("floor"));
        }
    }

    /**
     * json字符串-简单对象型与JSONObject之间的转换
     */
    public static void testJSONStrToJSONObject() {

        JSONObject jsonObject = JSON.parseObject(JSON_OBJ_STR);
        //JSONObject jsonObject1 = JSONObject.parseObject(JSON_OBJ_STR); //因为JSONObject继承了JSON，所以这样也是可以的

        System.out.println(jsonObject.getString("studentName") + ":" + jsonObject.getInteger("studentAge"));

    }

    public static void testJSONStrToJSONArray2() {
        String str2 = JSON_ARRAY_STR3.replaceAll("=", ":");
        JSONObject jsonObject = JSONObject.parseObject(str2);


        System.out.println(jsonObject.get("buildingFloorId") + ":" + jsonObject.get("floor"));

    }


}
