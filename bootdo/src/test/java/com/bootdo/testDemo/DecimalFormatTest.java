package com.bootdo.testDemo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RestController;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * @Author wukq
 * @Date: 2020/3/25
 * @Description:
 */
@RestController()
@RunWith(SpringRunner.class)
@SpringBootTest
public class DecimalFormatTest {

    @Test
    public void fun1() {
        DecimalFormat df = new DecimalFormat("0");
        df.setRoundingMode(RoundingMode.HALF_UP);
        System.out.println(df.format(0.625 * 100));
    }
}
