package com.bootdo.testDemo;

import com.bootdo.common.utils.DateUtils;
import org.junit.Test;

import java.util.Date;

/**
 * @Author wukq
 * @Date: 2020/7/21 10:47
 * @Description:
 */
public class TestTime {

  @Test
  public void testTime() {
    Date date = new Date();
    System.out.println(DateUtils.format(date, DateUtils.DATE_TIME_PATTERN));
  }


  @Test
  public void testTime2() {
    String date = "Mon, 20 Jul 2020 22:46:08";
    Date date1 = DateUtils.parseDate(date, DateUtils.DATE_TIME_PATTERN);//被 try catche返回new Date()了。
    System.out.println(DateUtils.format(date1, DateUtils.DATE_TIME_PATTERN));
  }
}
