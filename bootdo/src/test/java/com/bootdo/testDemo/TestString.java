package com.bootdo.testDemo;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author wukq
 * @Date: 2020/7/13 10:02
 * @Description:
 */

public class TestString {


  @Test
  public void test() {
    String[] array = {"1", "2", "3"};
    System.out.println("!");
    List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd", "", "jkl");
    List<String> filtered = strings.stream().filter(str -> !str.isEmpty()).collect(Collectors.toList());
    for (String str :
            filtered) {
      System.out.println(str);
    }
  }


  @Test
  public void test2() {
    int i = 0;
    System.out.println(i++);//0
    System.out.println(i++);//1
    System.out.println(i);//2
  }


  @Test
  public void test3() {
    String errorName = "123" + ",";
    errorName = errorName.substring(0, errorName.lastIndexOf(","));
    System.out.println(errorName);
  }


}
