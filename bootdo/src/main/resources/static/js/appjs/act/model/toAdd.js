var prefix = "/activiti/model";
$('#sub').on('click', function () {
    //debugger;
    var modelName = $('#modelName').val();
    var modelType = $('#modelType').val();
    var description = $('#description').val();

    var page = layer.open({
        type: 2,
        title: '新建模型',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['100%', '100%'],
        content: prefix + '/add' + "?" + "modelName=" + modelName + "&modelType=" + modelType + "&description=" + description,
        closeBtn: 0, //不显示关闭按钮
    });

})