package com.bootdo.entity;

import lombok.Data;

/**
 * @Author wukq
 * @Date: 2020/2/27
 * @Description:
 */
@Data
public class UserEntity {

    private long id;

    private String name;

    private Integer age;

    private String token;

    private String mobile;

}
