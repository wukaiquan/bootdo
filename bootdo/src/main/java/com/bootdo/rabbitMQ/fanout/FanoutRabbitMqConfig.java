package com.bootdo.rabbitMQ.fanout;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author wukq
 * @Date: 2021/4/10 17:43
 * @Description: fanout类型的Exchange路由规则非常简单，它会把所有发送到该Exchange的消息路由到所有与它绑定的Queue中，
 * 所以此时routing key是不起作用的。
 */
@Configuration
public class FanoutRabbitMqConfig {

  @Bean
  FanoutExchange fanoutExchange2() {
    return new FanoutExchange(FanoutConstant.FANOUT_EXCHANGE);
  }

  /**
   * 声明队列
   */
  @Bean
  public Queue fanoutQueue1() {
    // true表示持久化该队列
    return new Queue(FanoutConstant.QUEUE_FANOUT_TEST, true);
  }

  @Bean
  public Queue fanoutQueue2() {
    // true表示持久化该队列
    return new Queue(FanoutConstant.QUEUE_FANOUT_TEST2, true);
  }


  /**
   * 绑定队列,扇形交换西没有路由的概念，所以就没有了绑定路由的方法with(RabbitConstant.RK_SMS_PUSH)
   * 只绑定队列就可以了
   */
  @Bean
  public Binding bindingFanoutExchangeTest1() {
    return BindingBuilder.bind(fanoutQueue1()).to(fanoutExchange2());

  }

  /**
   * 不需要with路由
   */
  @Bean
  public Binding bindingFanoutExchangeTest2() {
    return BindingBuilder.bind(fanoutQueue2()).to(fanoutExchange2());

  }


}
