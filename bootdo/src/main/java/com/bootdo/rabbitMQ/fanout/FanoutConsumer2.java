package com.bootdo.rabbitMQ.fanout;

import com.alibaba.fastjson.JSONObject;
import com.bootdo.rabbitMQ.direct.DirectNews;
import com.bootdo.rabbitMQ.entity.MessageVo;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author wukq
 * @Date: 2021/4/10 17:43
 * @Description: 扇形交换机，只要队列绑定了消息，就会向队列里面放松，每一个消费者监听一个队列，就是订阅模式了
 * <p>
 * 当2个消费者都监听一个队列的时候，消息只会发送一个消费这，是队列的worker模式，但是因为队列2也绑定了交换机，所以队列2里面的消息没有被消费，
 * 会产生很大问题，所以又把消费者2绑定了队列2，因为队列已经持久化了，所以修改配置重启还是不生效，队列2依旧在，只有在服务器上删除队列，重启交换机
 * 修改配置才不在像队列里面发送消息
 * <p>
 * -----FanoutConsumer2 接受到的消息----
 * ----- FanoutConsumer1 接受到的消息----
 * -----FanoutConsumer2 接受到的消息----
 * ----- FanoutConsumer1 接受到的消息----
 * ----- FanoutConsumer1 接受到的消息----
 */
@Component
@RabbitListener(queues = FanoutConstant.QUEUE_FANOUT_TEST2)//监听队列
public class FanoutConsumer2 {

  @RabbitHandler
  public void process(String s, Message message, Channel channel) throws Exception {
    byte[] body = message.getBody();
    String bodyString = new String(body);

    MessageVo messageVo = JSONObject.parseObject(bodyString, MessageVo.class);
    System.out.println("-----FanoutConsumer2 接受到的消息----");
    String msg = messageVo.getMsg();
    List<DirectNews> directNews = JSONObject.parseArray(msg, DirectNews.class);
    for (DirectNews news : directNews) {
      // System.out.println("-----FanoutConsumer2 接受到的消息----" + news.toString());
    }


  }
}
