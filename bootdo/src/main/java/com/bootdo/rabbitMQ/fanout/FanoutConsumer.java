package com.bootdo.rabbitMQ.fanout;

import com.alibaba.fastjson.JSONObject;
import com.bootdo.rabbitMQ.direct.DirectNews;
import com.bootdo.rabbitMQ.entity.MessageVo;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author wukq
 * @Date: 2021/4/10 17:43
 * @Description:
 */
@Component
@RabbitListener(queues = FanoutConstant.QUEUE_FANOUT_TEST)//监听队列

public class FanoutConsumer {
  @RabbitHandler
  public void process(String s, Message message, Channel channel) throws Exception {


    byte[] body = message.getBody();
    String bodyString = new String(body);

    MessageVo messageVo = JSONObject.parseObject(bodyString, MessageVo.class);
    System.out.println("----- FanoutConsumer1 接受到的消息----");
    String msg = messageVo.getMsg();
    List<DirectNews> directNews = JSONObject.parseArray(msg, DirectNews.class);
    for (DirectNews news : directNews) {
      //System.out.println("----- FanoutConsumer1 接受到的消息----" + news.toString());
    }


  }
}
