package com.bootdo.rabbitMQ.fanout;

/**
 * @Author wukq
 * @Date: 2021/4/10 17:43
 * @Description:
 */
public class FanoutConstant {
  public static final String FANOUT_EXCHANGE = "fanout_exchange_2";
  public static final String QUEUE_FANOUT_TEST = "fanout_test_queue1";
  public static final String QUEUE_FANOUT_TEST2 = "fanout_test_queue2";
}
