package com.bootdo.rabbitMQ.direct;


import com.alibaba.fastjson.JSONObject;
import com.bootdo.rabbitMQ.entity.MessageVo;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @Author wukq
 * @Date: 2021/4/10 8:52
 * @Description: 自动确认
 */
@Component
//@RabbitListener(queues = DirectConstant.QUEUE_DIRECT)//监听队列
public class DirectConsumer2 {


  /**
   * No method found for class java.lang.String ,参数上必须加上String s
   * Listener method 'no match' threw exception,存储的时候是String，读取的时候也要是String
   * MessageVo messageVo = new MessageVo();
   * messageVo.setMsg(s);//存储的是string,所以读取的时候也要是String
   * s {"msg":"[{\"content\":\"放假也要写作业\",\"name\":\"小明\",\"title\":\"放假了\"},{\"content\":\"去襄阳旅游吃牛肉面，看古城\",\"name\":\"小王\",\"title\":\"去旅游\"},{\"content\":\"九宫山风景真不错啊\",\"name\":\"小李\",\"title\":\"去爬山\"}]"}
   */
  //@RabbitHandler
  public void process(String s, Message message, Channel channel) throws Exception {


    byte[] body = message.getBody();
    String bodyString = new String(body);

    MessageVo messageVo = JSONObject.parseObject(bodyString, MessageVo.class);
    System.out.println("-----------------messageVO" + messageVo.getMsg());
    String msg = messageVo.getMsg();
    List<DirectNews> directNews = JSONObject.parseArray(msg, DirectNews.class);
    for (DirectNews news : directNews) {
      System.out.println("-----接受到的消息----" + news.toString());
    }


  }
}
