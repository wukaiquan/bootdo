package com.bootdo.rabbitMQ.direct;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Author wukq
 * @Date: 2021/4/10 9:48
 * @Description:
 */
@Data
@ToString
public class DirectNews implements Serializable {


  private String name;
  private String title;
  private String content;

  /**
   * 阿里的fasltJSON,List<DirectNews> directNews = JSONObject.parseArray(msg, DirectNews.class);
   * JSONobject.parseArray必须要空的构造函数
   */
  public DirectNews() {
  }

  public DirectNews(String name, String title, String content) {
    this.name = name;
    this.title = title;
    this.content = content;
  }
}
