package com.bootdo.rabbitMQ.direct;

/**
 * @Author wukq
 * @Date: 2021/4/10 8:53
 * @Description: 消息确认机制
 */
public class DirectConstant {
  public static final String EXCHANGE = "direct_exchange_dev";//交换机
  public static final String QUEUE_DIRECT = "queue_direct";//队列
  public static final String QUEUE_PUSH = "queue_push";//路由
}
