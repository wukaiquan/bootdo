package com.bootdo.rabbitMQ.topic;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author wukq
 * @Date: 2021/4/10 12:02
 * @Description:
 */
@Configuration
public class TopicRabbitConfig {
  @Bean
  TopicExchange topicExchange2() {
    //消息持久化主要是将交换机、队列以及消息设置为durable = true(可持久化的)，要点主要有三个：
    return new TopicExchange(TopicConstant.TOPIC_EXCHANGE, true, false);
  }


  @Bean
  public Queue leftQueue() {
    // true表示持久化该队列，//durable：是否将队列持久化 true表示需要持久化 false表示不需要持久化
    return new Queue(TopicConstant.TOPIC_LEFT_QUEUE, true);
  }

  @Bean
  public Queue rightQueue() {
    return new Queue(TopicConstant.TOPIC_RIGHT_QUEUE, true);
  }

  @Bean
  public Binding bindLeftQueueExchange() {
    return BindingBuilder.bind(leftQueue()).to(topicExchange2()).with(TopicConstant.TOPIC_LEFT_QUEUE);
  }


  @Bean
  public Binding bindLeftRightExchange() {
    return BindingBuilder.bind(rightQueue()).to(topicExchange2()).with(TopicConstant.TOPIC_RIGHT_QUEUE);//队列绑定通配符路由

  }
}