package com.bootdo.rabbitMQ.topic;

/**
 * @Author wukq
 * @Date: 2021/4/10 16:21
 * @Description:
 */
public class TopicConstant {
  public static final String TOPIC_EXCHANGE = "top_exchange_2";
  public static final String TOPIC_LEFT_QUEUE = "topicqueue2.left";
  public static final String TOPIC_RIGHT_QUEUE = "topicqueue2.right";

  public static final String TOPIC_NOMAL_QUEUE = "topicqueue2.#";//topic.# 路由不能设置重复
}
