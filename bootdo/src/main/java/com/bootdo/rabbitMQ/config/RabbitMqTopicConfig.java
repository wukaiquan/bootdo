package com.bootdo.rabbitMQ.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author wukq
 * @Date: 2020/4/15 10:31
 * @Description:
 */
@Configuration

public class RabbitMqTopicConfig {
    /**
     * Topic Exchange
     * <p>
     * 主题交换机，这个交换机其实跟直连交换机流程差不多，但是它的特点就是在它的路由键和绑定键之间是有规则的。
     * * 简单地介绍下规则：
     * * <p>
     * * *  (星号) 用来表示一个单词 (必须出现的)
     * * #  (井号) 用来表示任意数量（零个或多个）单词
     * * 当一个队列的绑定键为 "#"（井号） 的时候，这个队列将会无视消息的路由键，接收所有的消息。
     */
    @Bean
    TopicExchange topicExchange() {
        //return new TopicExchange(RabbitConstant.TOPIC_EXCHANGE);
        //消息持久化主要是将交换机、队列以及消息设置为durable = true(可持久化的)，要点主要有三个：
        return new TopicExchange(RabbitConstant.TOPIC_EXCHANGE, true, false);
    }


    @Bean
    public Queue firstQueue() {
        // true表示持久化该队列，//durable：是否将队列持久化 true表示需要持久化 false表示不需要持久化
        return new Queue(RabbitConstant.TOPIC_DIRECT, true);
    }

    @Bean
    public Queue secondQueue() {
        // true表示持久化该队列
        return new Queue(RabbitConstant.TOPIC_DIRECT2, true);
    }

    /**
     * 将firstQueue和topicExchange绑定,而且绑定的键值为topic.man
     **/
    @Bean
    public Binding bindingExchangeMessage() {
        return BindingBuilder.bind(firstQueue()).to(topicExchange()).with(RabbitConstant.TOPIC_DIRECT);
    }

    /**
     * 将secondQueue和topicExchange绑定,而且绑定的键值为用上通配路由键规则topic.#
     * 这样只要是消息携带的路由键是以topic.开头,都会分发到该队列
     * topic.woman这个队列接受所有topic路由开头的消息，所以，topic.man的路由消息也能收到，
     * 但是topic.man这个队列只能接受到topic.man这个路由的消息，所以接收不到topic.woman这个路由的
     * 消息
     */
    @Bean
    Binding bindingExchangeMessage2() {

        return BindingBuilder.bind(secondQueue()).to(topicExchange()).with("topic.#");
    }


}
