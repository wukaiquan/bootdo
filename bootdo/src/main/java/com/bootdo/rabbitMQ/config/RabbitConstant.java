package com.bootdo.rabbitMQ.config;

/**
 * @Author wukq
 * @Date: 2020/4/11
 * @Description:
 */
public class RabbitConstant {

    /**
     * 交换机名称,RabbitMqConfig代码可以直接新建交换机exchange_wukq
     * 实例化的时候用的时 new DirectExchange();
     * 直连型交换机，根据消息携带的路由键将消息投递给对应队列。大致流程，有一个队列绑定到一个直连交换机上，同时赋予一个路由键 routing key 。
     * 然后当一个消息携带着路由值为X，这个消息通过生产者发送给交换机时，交换机就会根据这个路由值X去寻找绑定值也是X的队列。
     */
    public final static String EXCHANGE = "exchange_wukq";

    /**
     * Fanout Exchange
     * 扇型交换机，这个交换机没有路由键概念，就算你绑了路由键也是无视的。 这个交换机在接收到消息后，会直接转发到绑定到它上面的所有队列。
     */
    public final static String FANOUT_EXCHANGE = "fanout_exchange_wukq";


    /**
     * Topic Exchange
     * <p>
     * 主题交换机，这个交换机其实跟直连交换机流程差不多，但是它的特点就是在它的路由键和绑定键之间是有规则的。
     */
    public final static String TOPIC_EXCHANGE = "topic_exchange_wukq";

    //队列
    public final static String QUEUE_TRANSACTION = "queue_transaction";
    public final static String QUEUE_LOGIN = "queue_login";
    public final static String QUEUE_WARNING_MSG = "queue_warning_msg";
    public final static String QUEUE_LAST_LOGIN = "queue_last_login";
    public final static String QUEUE_UPLOAD_DATA = "queue_upload_data";//数据上传
    public final static String QUEUE_UPLOAD_DATA_DEAD = "queue_upload_data_dead";//数据上传 - 死信队列
    public final static String QUEUE_REQUEST_CODE = "queue_request_code";//异步查询
    public final static String QUEUE_REQUEST_CODE_DEAD = "queue_request_code_dead";//异步查询的死信队列
    public final static String QUEUE_MESSAGE_PUSH = "queue_message_push";
    public final static String QUEUE_SMS_PUSH = "queue_sms_push";
    public final static String QUEUE_VIEW_TIMES = "queue_view_times";
    public final static String QUEUE_RENT_PUBLISH = "queue_rent_publish";//设备租赁, 发布消息
    public final static String QUEUE_RENT_PUBLISH_DEAD = "queue_rent_publish_dead";//设备租赁, 发布消息 死信队列
    public final static String QUEUE_REAL_SYSTEM = "queue_real_system";//劳务实名制
    public final static String QUEUE_REAL_SYSTEM_DEAD = "queue_real_system_dead";//劳务实名制 死信队列

    //fanout用的队列
    public final static String QUEUE_FANOUT_TEST = "queue_fanout_test";
    public final static String QUEUE_FANOUT_TEST2 = "queue_fanout_test2";
    public final static String QUEUE_FANOUT_TEST3 = "queue_fanout_test3";

    //topic用的队列
    public final static String QUEUE_TOPIC_TEST = "queue_topic_test";


    //路由key
    public final static String RK_TRANSACTION = "transaction";
    public final static String RK_LOGIN = "login";
    public final static String RK_WARNING_MSG = "warningMsg";
    public final static String RK_LAST_LOGIN = "lastLogin";//最后一次登录
    public final static String RK_UPLOAD_DATA = "queueUploadData";//上传数据至武汉实名制平台
    public final static String RK_UPLOAD_DATA_DEAD = "queueUploadDataDead";//上传数据至武汉实名制平台 - 死信队列
    public final static String RK_REQUEST_CODE = "queueRequestCode";//武汉实名制平台返回的请求序列号 路由键
    public final static String RK_REQUEST_CODE_DEAD = "queueRequestCodeDead";//武汉实名制平台返回的请求序列号
    public final static String RK_MESSAGE_PUSH = "queueMessagePush";//消息推送
    public final static String RK_SMS_PUSH = "queueSmsPush";//短信平台
    public final static String RK_VIEW_TIMES = "queueViewTimes";//浏览次数
    public final static String RK_RENT_PUBLISH = "rentPublish";//设备租赁, 发布消息
    public final static String RK_RENT_PUBLISH_DEAD = "rentPublishDead";//设备租赁, 发布消息 死信队列
    public final static String RK_REAL_SYSTEM = "queueRealSystem";//劳务实名制
    public final static String RK_REAL_SYSTEM_DEAD = "queueRealSystemDead";//劳务实名制 死信队列


    //topic路由key
    public final static String TOPIC_DIRECT = "topic.man";
    public final static String TOPIC_DIRECT2 = "topic.woman";

}
