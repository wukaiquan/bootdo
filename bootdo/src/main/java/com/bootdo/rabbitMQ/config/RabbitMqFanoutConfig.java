package com.bootdo.rabbitMQ.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author wukq
 * @Date: 2020/4/15 10:32
 * @Description: 扇形交换机，只要队列绑定了交换机就能接受到消息
 */
@Configuration
public class RabbitMqFanoutConfig {

    /**
     * 生命fanout交换机
     * 扇型交换机，这个交换机没有路由键概念，就算你绑了路由键也是无视的。 这个交换机在接收到消息后，会直接转发到绑定到它上面的所有队列。
     */
    @Bean
    FanoutExchange fanoutExchange() {
        return new FanoutExchange(RabbitConstant.FANOUT_EXCHANGE);
    }


    /**
     * 声明队列
     */
    @Bean
    public Queue queueFanout1() {
        // true表示持久化该队列
        return new Queue(RabbitConstant.QUEUE_FANOUT_TEST, true);
    }

    @Bean
    public Queue queueFanout2() {
        // true表示持久化该队列
        return new Queue(RabbitConstant.QUEUE_FANOUT_TEST2, true);
    }

    @Bean
    public Queue queueFanout3() {
        // true表示持久化该队列
        return new Queue(RabbitConstant.QUEUE_FANOUT_TEST3, true);
    }

    /**
     * 绑定队列,扇形交换西没有路由的概念，所以就没有了绑定路由的方法with(RabbitConstant.RK_SMS_PUSH)
     * 只绑定队列就可以了
     */
    @Bean
    public Binding bindingExchange() {
        return BindingBuilder.bind(queueFanout1()).to(fanoutExchange());

    }

    @Bean
    public Binding bindingExchange2() {
        return BindingBuilder.bind(queueFanout2()).to(fanoutExchange());

    }

    @Bean
    public Binding bindingExchange3() {
        return BindingBuilder.bind(queueFanout3()).to(fanoutExchange());

    }
}
