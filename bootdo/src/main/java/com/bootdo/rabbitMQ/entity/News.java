package com.bootdo.rabbitMQ.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
@ApiModel("消息")
@Data
public class News {

    private Long newsId;//消息信息ID

    private String labelType;//标签类型 1:审批 2:任务 3:质量 4:报警 10:系统
    private String labelDetailType;//子标签类型

    private String title;//标题
    private Long subContractorId;//企业信息ID
    private Long projectMasterId;//项目ID
    private String projectName;//项目名称
    private Long moduleRecId;//对应模块信息主键（ID），审批对应 PROCESS_RESULT_ID（审批结果ID）
    private Long createUserId;//创建人的用户ID
    private String content;//消息内容
    private String handleStatus;//处理状态 (1未处理, 2已处理)
    private Long companyEmployeeId;//接收人的（聘用关系主键）
    private Long userId;//接收人的用户ID
    private String createTime;//创建时间

    //消息类型 (短信/推送消息)  PUSHMESSAGE("站内信/推送", 1),SHORTMESSAGE("短信", 2);
    private Integer msgInfoType;
    /**
     * 接收用户信息UserId
     **/
    private Long jPushUserId;
    //消息类别 (一般消息/预警消息)   COMMONLY("一般消息", "commonly"), WARNING("预警消息", "warning");
    private String msgType;
    /**
     * 接收用户电话号码
     **/
    private String receivePhone;

    private String isRisk;

    private Map<String, String> paramMap;

    public News() {
    }

    public News(String labelType, String labelDetailType, String title, Long subContractorId, Long projectMasterId, Long moduleRecId,
                Long companyEmployeeId, String content, String handleStatus) {
        this.labelType = labelType;
        this.labelDetailType = labelDetailType;
        this.title = title;
        this.subContractorId = subContractorId;
        this.projectMasterId = projectMasterId;
        this.moduleRecId = moduleRecId;
        this.companyEmployeeId = companyEmployeeId;
        this.content = content;
        this.handleStatus = handleStatus;
        this.userId = null;
    }

    public News(String labelType, String labelDetailType, String title, Long subContractorId, Long moduleRecId,
                Long companyEmployeeId, String content, String handleStatus, Long userId, String createTime) {
        this.labelType = labelType;
        this.labelDetailType = labelDetailType;
        this.title = title;
        this.subContractorId = subContractorId;
        this.moduleRecId = moduleRecId;
        this.companyEmployeeId = companyEmployeeId;
        this.content = content;
        this.handleStatus = handleStatus;
        this.userId = userId;
        this.createTime = createTime;
    }

    /**
     * @param labelType
     * @param labelDetailType
     * @param title
     * @param subContractorId
     * @param moduleRecId
     * @param companyEmployeeId
     * @param content
     * @param handleStatus
     * @param userId
     * @param createTime
     * @param msgInfoType
     * @param jPushUserId
     * @param msgType
     */
    public News(String labelType, String labelDetailType, String title, Long subContractorId, Long projectMasterId, Long moduleRecId,
                Long companyEmployeeId, String content, String handleStatus, Long userId, Long createUserId, String createTime,
                Integer msgInfoType, Long jPushUserId, String msgType, String receivePhone) {
        this.labelType = labelType;
        this.labelDetailType = labelDetailType;
        this.title = title;
        this.subContractorId = subContractorId;
        this.projectMasterId = projectMasterId;
        this.moduleRecId = moduleRecId;
        this.companyEmployeeId = companyEmployeeId;
        this.content = content;
        this.handleStatus = handleStatus;
        this.userId = userId;
        this.createUserId = createUserId;
        this.createTime = createTime;
        this.msgInfoType = msgInfoType;
        this.jPushUserId = jPushUserId;
        this.msgType = msgType;
        this.receivePhone = receivePhone;
    }

    /**
     * @param content      消息主体内容
     * @param userId       发送人Id
     * @param msgInfoType  消息类型 (短信/站内信)
     * @param msgType      消息类别 (一般消息/预警消息)
     * @param receivePhone 接收用户手机号码
     */
    public News(String content, Long userId, Integer msgInfoType, String msgType, String receivePhone) {
        this.content = content;
        this.userId = userId;
        this.msgInfoType = msgInfoType;
        this.msgType = msgType;
        this.receivePhone = receivePhone;
    }


    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getLabelType() {
        return labelType;
    }

    public void setLabelType(String labelType) {
        this.labelType = labelType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getSubContractorId() {
        return subContractorId;
    }

    public void setSubContractorId(Long subContractorId) {
        this.subContractorId = subContractorId;
    }

    public Long getModuleRecId() {
        return moduleRecId;
    }

    public void setModuleRecId(Long moduleRecId) {
        this.moduleRecId = moduleRecId;
    }

    public Long getCompanyEmployeeId() {
        return companyEmployeeId;
    }

    public void setCompanyEmployeeId(Long companyEmployeeId) {
        this.companyEmployeeId = companyEmployeeId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHandleStatus() {
        return handleStatus;
    }

    public void setHandleStatus(String handleStatus) {
        this.handleStatus = handleStatus;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }


    public String getLabelDetailType() {
        return labelDetailType;
    }

    public void setLabelDetailType(String labelDetailType) {
        this.labelDetailType = labelDetailType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getProjectMasterId() {
        return projectMasterId;
    }

    public void setProjectMasterId(Long projectMasterId) {
        this.projectMasterId = projectMasterId;
    }

    public Integer getMsgInfoType() {
        return msgInfoType;
    }

    public void setMsgInfoType(Integer msgInfoType) {
        this.msgInfoType = msgInfoType;
    }

    public Long getjPushUserId() {
        return jPushUserId;
    }

    public void setjPushUserId(Long jPushUserId) {
        this.jPushUserId = jPushUserId;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getReceivePhone() {
        return receivePhone;
    }

    public void setReceivePhone(String receivePhone) {
        this.receivePhone = receivePhone;
    }

    public void putParam(String key, String value) {
        if (this.paramMap == null) {
            this.paramMap = new HashMap<>();
        }
        this.paramMap.put(key, value);
    }
}
