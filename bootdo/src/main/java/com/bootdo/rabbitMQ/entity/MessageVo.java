package com.bootdo.rabbitMQ.entity;

import lombok.Data;

/**
 * @Author wukq
 * @Date: 2020/4/13
 * @Description:
 */
@Data
public class MessageVo {

    private String msgId;//消息ID

    private String receiveUserId; //接收信息的用户ID

    private String routingKey;//路由键

    private String msg;//要发送的消息 , 自定义json,


    private String queue;//扇形交换机需要用到的队列


}
