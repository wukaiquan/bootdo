package com.bootdo.rabbitMQ.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;

import java.util.Map;

/**
 * @Author wukq
 * @Date: 2020/4/15 16:58
 * @Description: 这样消费消息确实比RabbitSMSPushConsumer见到那
 */
//@Component
//@RabbitListener(queues = RabbitConstant.QUEUE_FANOUT_TEST)
public class FanoutReceiverA {
    @RabbitHandler
    public void process(Map testMessage) {
        /**
         * FanoutReceiverA消费者收到消息  : {createTime=2020-04-16 12:12:57, phone=123456798, messageId=978d7b41-d900-49bd-b4ab-2cdc7c42f082, content=test, messageData=message: testFanoutMessage }
         * 这个可以接受到消息,RabbitFanoutConsumer1 接受不到消息。发送的map，接收的是乱码？
         * 这个直接用Map接受倒是可以
         * */
        System.out.println("FanoutReceiverA消费者收到消息  : " + testMessage.toString());
    }

}
