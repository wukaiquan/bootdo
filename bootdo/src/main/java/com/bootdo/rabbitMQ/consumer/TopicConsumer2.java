package com.bootdo.rabbitMQ.consumer;

import com.bootdo.rabbitMQ.config.RabbitConstant;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author wukq
 * @Date: 2020/4/20 15:12
 * @Description:
 */
@Component
@RabbitListener(queues = RabbitConstant.TOPIC_DIRECT2)
public class TopicConsumer2 {

    @RabbitHandler
    public void process(Map message) {
        System.out.println("TopicConsumer2消费者收到消息  : " + message.toString() + " messageID: " + message.get("messageId") +
                " messageData: " + message.get("messageData"));

    }


}
