package com.bootdo.rabbitMQ.consumer;

import com.bootdo.rabbitMQ.config.RabbitConstant;
import com.bootdo.web.service.SendSMSService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author wukq
 * @Date: 2020/4/20 14:42
 * @Description:
 */
@Component
@RabbitListener(queues = RabbitConstant.TOPIC_DIRECT)
public class TopicConsumer1 {

    @Autowired
    private SendSMSService sendSMSService;

    @RabbitHandler
    public void process(Map message) {


        System.out.println("TopicConsumer1消费者收到消息  : " + message.toString() + " messageID: " + message.get("messageId") +
                " messageData: " + message.get("messageData"));
        //sendSMSService.sendMsg(message.get("phone").toString(), message.get("content").toString());

    }

}
