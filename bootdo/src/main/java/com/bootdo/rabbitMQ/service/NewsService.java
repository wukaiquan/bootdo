package com.bootdo.rabbitMQ.service;

import com.bootdo.rabbitMQ.entity.News;

public interface NewsService {

    public void sendMessageList(News news, Integer msgInfoType, String msgType);

    public void sendMessageList2(News news, Integer msgInfoType, String msgType);

    public void sendMessageList3(News news, Integer msgInfoType, String msgType);


}
