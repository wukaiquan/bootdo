package com.bootdo.rabbitMQ.service;

import com.bootdo.rabbitMQ.annos.RabbitmqTag;
import com.bootdo.rabbitMQ.entity.News;
import com.bootdo.rabbitMQ.producer.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author wukq
 * @Date: 2020/4/14 16:44
 * @Description:
 */
@Service
public class NewsServiceImpl implements NewsService {
    @Autowired
    private Sender sender;

    /**
     * @param news        单条news 消息
     * @param msgInfoType SendMessageType.PUSHMESSAGE.getValue  msgInfoType (1:需要推送的消息 2:短信,3:写入t_news表,但不发消息)
     * @param msgType     消息类型 NewsTypeCategory.COMMONLY.getValue (1:一般消息 2:预警消息)
     */
    @RabbitmqTag
    @Override
    public void sendMessageList(News news, Integer msgInfoType, String msgType) {
        List<News> list = new ArrayList<>();
        list.add(news);

        if (msgInfoType.equals(1) || msgInfoType.equals(3)) {
            // newsMapper.insertNewsList(list);
            System.out.println("-------------------------");
        }
        news.setjPushUserId(news.getUserId());
        news.setMsgInfoType(msgInfoType);
        news.setMsgType(msgType);
    }


    @Override
    public void sendMessageList2(News news, Integer msgInfoType, String msgType) {
        List<News> list = new ArrayList<>();
        list.add(news);

        if (msgInfoType.equals(1) || msgInfoType.equals(3)) {
            // newsMapper.insertNewsList(list);
            System.out.println("-------------------------");
        }
        news.setjPushUserId(news.getUserId());
        news.setMsgInfoType(msgInfoType);
        news.setMsgType(msgType);
    }


    @Override
    public void sendMessageList3(News news, Integer msgInfoType, String msgType) {
        List<News> list = new ArrayList<>();
        list.add(news);

        if (msgInfoType.equals(1) || msgInfoType.equals(3)) {
            // newsMapper.insertNewsList(list);
            System.out.println("-------------------------");
        }
        news.setjPushUserId(news.getUserId());
        news.setMsgInfoType(msgInfoType);
        news.setMsgType(msgType);
    }

}
