package com.bootdo.rabbitMQ.annos;

import java.lang.annotation.*;

@Inherited
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RabbitmqTag {

    String name() default "";
}

