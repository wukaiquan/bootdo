package com.bootdo.rabbitMQ.annos;

import java.lang.annotation.*;

/**
 * @Author wukq
 * @Date: 2020/4/15 10:40
 * @Description:
 */
@Inherited
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RabbitmqFanoutTag {
    String name() default "";
}
