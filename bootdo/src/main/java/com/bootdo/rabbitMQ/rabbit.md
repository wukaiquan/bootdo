RabbitMQ的交换机类型共有四种，是根据其路由过程的不同而划分成的
分别是Direct Exchange（直连交换机）， Fanout Exchange（扇型交换机）， Topic Exchange（主题交换机）与 Headers Exchange（头交换机）

acknowledge-mode: manual
AUTO：consumer自动应答，处理成功（注意：此处的成功确认是没有发生异常）发出ack，处理失败发出nack。rabbitmq发出消息后会等待consumer端应答，只有收到ack确定信息后才会将消息在rabbitmq清除掉。收到nack异常信息的处理方法由setDefaultRequeueReject()方法设置，这种模式下，发送错误的消息可以恢复。
MANUAL：基本等同于AUTO模式，区别是需要人为调用方法确认。


服务重启后，消息没有丢失，标识消息持久化了
开启2个消费之后，消费消息的能力明显上升，2个消费者都是监听一个队列
自动确认的consumer能增加吞吐量，手动确认的consumer吞吐量明细那减少。
当手动确认的时候队列里面的消息太多，还会报错。


RabbitMQ 15672控制台详解
overview→Totals	所有队列的阻塞情况
Ready	待消费的消息总数
Unacked	待应答的消息总数
Total	总数 Ready+Unacked

Publish	producter pub消息的速率
Publisher confirm	broker确认pub消息的速率
Deliver(manual ack)	ustomer手动确认的速率
Deliver( auto ack)	customer自动确认的速率
Consumer ack	customer正在确认的速率

fanout
fanout类型的Exchange路由规则非常简单，它会把所有发送到该Exchange的消息路由到所有与它绑定的Queue中，所以此时routing key是不起作用的。
上图中，生产者（P）发送到Exchange（X）的所有消息都会路由到图中的两个Queue，并最终被两个消费者（C1与C2）消费。
扇形交换机，只要队列绑定了消息，就会向队列里面放松，是一个发布订阅模式

direct
direct类型的Exchange路由规则也很简单，它会把消息路由到那些binding key与routing key完全匹配的Queue中。
以上图的配置为例，我们以routingKey=”error”发送消息到Exchange，则消息会路由到Queue1（amqp.gen-S9b…，这是由RabbitMQ自动生成的Queue名称）
和Queue2（amqp.gen-Agl…）；如果我们以routingKey=”info”或routingKey=”warning”来发送消息，则消息只会路由到Queue2。
如果我们以其他routingKey发送消息，则消息不会路由到这两个Queue中。

topic
前面讲到direct类型的Exchange路由规则是完全匹配binding key与routing key，但这种严格的匹配方式在很多情况下不能满足实际业务需求。topic类
型的Exchange在匹配规则上进行了扩展，它与direct类型的Exchage相似，也是将消息路由到binding key与routing key相匹配的Queue中，但这里的匹配
规则有些不同，它约定：
routing key为一个句点号“. ”分隔的字符串（我们将被句点号“. ”分隔开的每一段独立的字符串称为一个单词），如“stock.usd.nyse”、“nyse.vmw”、
“quick.orange.rabbit”
binding key与routing key一样也是句点号“. ”分隔的字符串
binding key中可以存在两种特殊字符“*”与“#”，用于做模糊匹配，其中“*”用于匹配一个单词，“#”用于匹配多个单词（可以是零个）


rabbit队列
RabbitMQ详解（三）------RabbitMQ的五种队列
目录

1、简单队列
2、work 模式
3、发布/订阅模式
4、路由模式






