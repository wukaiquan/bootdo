package com.bootdo.system.controller;

import com.bootdo.config.RedisUtil;
import com.bootdo.entity.UserEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author wukq
 * @Date: 2020/03/27
 * @Description:
 */

@RestController
@RequestMapping("/web/redis/test")
public class RedisController {
    private static int ExpireTime = 60;   // redis中存储的过期时间60s

    @Resource
    private RedisUtil redisUtil;

    @RequestMapping("/set")
    public boolean redisset(String key, String value) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(Long.valueOf(1));
        userEntity.setName("zhangsan");
        userEntity.setAge(20);
        userEntity.setMobile("136992275646");


        return redisUtil.set(key, value);
    }

    @RequestMapping("/get")
    public Object redisget(String key) {
        return redisUtil.get(key);
    }

    @RequestMapping("/expire")
    public boolean expire(String key) {
        return redisUtil.expire(key, ExpireTime);
    }
}
