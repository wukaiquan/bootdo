package com.bootdo.system.controller;

import com.bootdo.common.config.Constant;
import com.bootdo.common.controller.BaseController;
import com.bootdo.common.domain.Tree;
import com.bootdo.common.utils.R;
import com.bootdo.rabbitMQ.config.RabbitConstant;
import com.bootdo.rabbitMQ.direct.DirectNews;
import com.bootdo.rabbitMQ.entity.News;
import com.bootdo.rabbitMQ.producer.Sender;
import com.bootdo.rabbitMQ.service.NewsService;
import com.bootdo.system.domain.DeptDO;
import com.bootdo.system.service.DeptService;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 部门管理
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-27 14:40:36
 */

@Controller
@RequestMapping("/system/sysDept")
public class DeptController extends BaseController {
	private String prefix = "system/dept";
	@Autowired
	private DeptService sysDeptService;
	@Autowired
	private NewsService newsService;
	@Autowired
	private RabbitTemplate rabbitTemplate;


	@Autowired
	private Sender sender;


	@GetMapping()
	@RequiresPermissions("system:sysDept:sysDept")
	String dept() {
		return prefix + "/dept";
	}

	@ApiOperation(value="获取部门列表", notes="")
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:sysDept:sysDept")
	public List<DeptDO> list() {
		Map<String, Object> query = new HashMap<>(16);
		List<DeptDO> sysDeptList = sysDeptService.list(query);
		return sysDeptList;
	}

	@GetMapping("/add/{pId}")
	@RequiresPermissions("system:sysDept:add")
	String add(@PathVariable("pId") Long pId, Model model) {
		model.addAttribute("pId", pId);
		if (pId == 0) {
			model.addAttribute("pName", "总部门");
		} else {
			model.addAttribute("pName", sysDeptService.get(pId).getName());
		}
		return  prefix + "/add";
	}

	@GetMapping("/edit/{deptId}")
	@RequiresPermissions("system:sysDept:edit")
	String edit(@PathVariable("deptId") Long deptId, Model model) {
		DeptDO sysDept = sysDeptService.get(deptId);
		model.addAttribute("sysDept", sysDept);
		if(Constant.DEPT_ROOT_ID.equals(sysDept.getParentId())) {
			model.addAttribute("parentDeptName", "无");
		}else {
			DeptDO parDept = sysDeptService.get(sysDept.getParentId());
			model.addAttribute("parentDeptName", parDept.getName());
		}
		return  prefix + "/edit";
	}

	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("system:sysDept:add")
	public R save(DeptDO sysDept) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (sysDeptService.save(sysDept) > 0) {
			return R.ok();
		}
		return R.error();
	}

	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:sysDept:edit")
	public R update(DeptDO sysDept) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		if (sysDeptService.update(sysDept) > 0) {
			return R.ok();
		}
		return R.error();
	}

	/**
	 * 删除
	 */
	@PostMapping("/remove")
	@ResponseBody
	@RequiresPermissions("system:sysDept:remove")
	public R remove(Long deptId) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("parentId", deptId);
		if(sysDeptService.count(map)>0) {
			return R.error(1, "包含下级部门,不允许修改");
		}
		if(sysDeptService.checkDeptHasUser(deptId)) {
			if (sysDeptService.remove(deptId) > 0) {
				return R.ok();
			}
		}else {
			return R.error(1, "部门包含用户,不允许修改");
		}
		return R.error();
	}

	/**
	 * 删除
	 */
	@PostMapping("/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:sysDept:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] deptIds) {
		if (Constant.DEMO_ACCOUNT.equals(getUsername())) {
			return R.error(1, "演示系统不允许修改,完整体验请部署程序");
		}
		sysDeptService.batchRemove(deptIds);
		return R.ok();
	}

	@GetMapping("/tree")
	@ResponseBody
	public Tree<DeptDO> tree() {
		Tree<DeptDO> tree = new Tree<DeptDO>();
		tree = sysDeptService.getTree();
		return tree;
	}

	@GetMapping("/treeView")
	String treeView() {
		return  prefix + "/deptTree";
	}


	/**
	 * direct交换机
	 */
	@ResponseBody
	@RequestMapping("/sendRabbitMQ")
	@RequiresPermissions("system:sysDept:edit")
	public R sendRabbitMQ(DeptDO sysDept) {
//多线程发送，消息队列里面最多接收到了100万消息，然后慢慢的消费
		//虽然线程池设置了100个线程，但是线程池只有一个线程再跑，	executorService.submit() 提交了一个线程，多写几个，就多提交了几个线程了
//		ExecutorService executorService = Executors.newFixedThreadPool(100);//FixedThreadPool 容量固定的线程池
//		executorService.submit(new Runnable() {
//			@Override
//			public void run() {
//				for (int i = 0; i < 10; i++) {
//					News news = new News();
//					news.setReceivePhone("13692272649");
//					news.setContent("test===============");
//					newsService.sendMessageList(news, 2, "1");
//				}
//				System.out.println("id:" + Thread.currentThread().getId() + "  name:" + Thread.currentThread().getName());
//			}
//		});
//		executorService.shutdown();


		News news = new News();
		news.setReceivePhone("13692272649");
		news.setContent("test===============");
		newsService.sendMessageList(news, 2, "1");
		return R.ok();

	}

	@ResponseBody
	@RequestMapping("/directSendNews")
	@RequiresPermissions("system:sysDept:edit")
	public R directSendNews() {
		List<DirectNews> list = new ArrayList<DirectNews>();
		DirectNews news = new DirectNews("小明", "放假了", "放假也要写作业");
		DirectNews news2 = new DirectNews("小王", "去旅游", "去襄阳旅游吃牛肉面，看古城");
		DirectNews news3 = new DirectNews("小李", "去爬山", "九宫山风景真不错啊");
		list.add(news);
		list.add(news2);
		list.add(news3);
		for (int i = 0; i < 10; i++) {
			sender.sendDirect(list);
		}
		return R.ok();

	}
	/**
	 * fanout交换机
	 */
	@ResponseBody
	@RequestMapping("/sendRabbitMQ2")
	@RequiresPermissions("system:sysDept:edit")
	public R sendRabbitMQ2(DeptDO sysDept) {
		String messageId = String.valueOf(UUID.randomUUID());
		String messageData = "message: testFanoutMessage ";
		String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		Map<String, Object> map = new HashMap<>();
		map.put("messageId", messageId);
		map.put("messageData", messageData);
		map.put("createTime", createTime);
		map.put("phone", "123456798");
		map.put("content", "test");
		rabbitTemplate.convertAndSend(RabbitConstant.FANOUT_EXCHANGE, null, map);
		return R.ok();

	}

	/**
	 * fanout交换机2
	 */
	@ResponseBody
	@RequestMapping("/sendfanoutRabbitMQ2")
	@RequiresPermissions("system:sysDept:edit")
	public R sendfanoutRabbitMQ2(DeptDO sysDept) {
		List<DirectNews> list = new ArrayList<DirectNews>();
		DirectNews news = new DirectNews("张三", "喝酒", "喝酒配下酒菜");
		DirectNews news2 = new DirectNews("李四", "烧烤", "啤酒配烧烤");
		DirectNews news3 = new DirectNews("王五", "火锅", "冰饮配火锅");
		list.add(news);
		list.add(news2);
		list.add(news3);
		for (int i = 0; i < 5; i++) {
			sender.sendfanout(list);
		}
		return R.ok();

	}
	/**
	 * topic交换机1
	 */
	@ResponseBody
	@RequiresPermissions("system:sysDept:edit")
	@RequestMapping("/sendTopicMessage1")
	public R sendTopicMessage1() {
		String messageId = String.valueOf(UUID.randomUUID());
		String messageData = "message:man";
		String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		Map<String, Object> manMap = new HashMap<>();
		manMap.put("messageId", messageId);
		manMap.put("messageData", messageData);
		manMap.put("createTime", createTime);
		rabbitTemplate.convertAndSend(RabbitConstant.TOPIC_EXCHANGE, RabbitConstant.TOPIC_DIRECT, manMap);//路由是topic.man
		return R.ok();
	}

	/**
	 * topic交换机2
	 */
	@ResponseBody
	@RequiresPermissions("system:sysDept:edit")
	@RequestMapping("/sendTopicMessage2")
	public R sendTopicMessage2() {
		String messageId = String.valueOf(UUID.randomUUID());
		String messageData = "message: woman is all ";
		String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		Map<String, Object> womanMap = new HashMap<>();
		womanMap.put("messageId", messageId);
		womanMap.put("messageData", messageData);
		womanMap.put("createTime", createTime);
		rabbitTemplate.convertAndSend(RabbitConstant.TOPIC_EXCHANGE, RabbitConstant.TOPIC_DIRECT2, womanMap);//路由是topic.woman
		return R.ok();
	}

	/**
	 * topic交换机3
	 */
	@ResponseBody
	@RequiresPermissions("system:sysDept:edit")
	@RequestMapping("/sendTopicMessage3")
	public R sendTopicMessage3() {

		List<DirectNews> list = new ArrayList<DirectNews>();
		DirectNews news = new DirectNews("张三", "喝酒", "喝酒配下酒菜");
		DirectNews news2 = new DirectNews("李四", "烧烤", "啤酒配烧烤");
		DirectNews news3 = new DirectNews("王五", "火锅", "冰饮配火锅");
		list.add(news);
		list.add(news2);
		list.add(news3);
		for (int i = 0; i < 50; i++) {
			sender.sendTopic(list);
		}

		return R.ok();
	}
}
