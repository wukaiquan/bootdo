package com.bootdo.elasticsearch.anno;

import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EsCloumn {

    String name() default "";

    String array() default "0";

    Class clazz() default void.class;

    String dateformat() default "";
}
