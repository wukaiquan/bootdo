package com.bootdo.elasticsearch.util;


import lombok.Data;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregations;

import java.util.List;
import java.util.Map;

@Data
public class EsResult<T> {

    private long total;
    private Aggregations aggregations;
    private List<T> esResultList;

    public static EsResult getEsResult(List esResultList, SearchResponse searchResponse) {
        SearchHits hits = searchResponse.getHits();
        EsResult esResult = new EsResult();
        esResult.setTotal(hits.getTotalHits());
        esResult.setAggregations(searchResponse.getAggregations());

        if (esResultList != null && esResultList.size() > 0 && (esResultList.get(0) instanceof Map)) {
            //将map里面的key,转成驼峰规则命名
            List newList = Conv.formatHumpNameForList(esResultList);
            esResult.setEsResultList(newList);
        } else {
            esResult.setEsResultList(esResultList);
        }

        return esResult;
    }
}
