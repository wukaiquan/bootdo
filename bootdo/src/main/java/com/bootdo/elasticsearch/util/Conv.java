package com.bootdo.elasticsearch.util;

import org.apache.commons.lang3.time.DateUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 类型转换工具类 Created by xp on 2018/3/29.
 */
public class Conv {

    private static final SimpleDateFormat FORMAT_DATE_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat FORMAT_TIME = new SimpleDateFormat("HH:mm:ss");
    private static final SimpleDateFormat FORMAT_DATE = new SimpleDateFormat("yyyy-MM-dd");

    public static String createUuid() {
        return UUID.randomUUID().toString().replace("-", "").toLowerCase();
    }

    public static int NI(Object o) {
        return NI(o, 0);
    }

    public static int NI(Object o, int df) {
        if (o == null) {
            return df;
        }
        if (o instanceof Integer) {
            return (Integer) o;
        }
        if (o instanceof Byte) {
            return (Byte) o;
        }
        if (o instanceof Short) {
            return (Short) o;
        }
        if (o instanceof BigInteger) {
            return ((BigInteger) o).intValue();
        }
        if (o instanceof Double) {
            return ((Double) o).intValue();
        }
        if (o instanceof BigDecimal) {
            return ((BigDecimal) o).intValue();
        }
        try {
            return Integer.parseInt(o.toString());
        } catch (Exception e) {
            return df;
        }
    }

    public static Integer NIorNull(Object o, Integer df) {
        if (o == null) {
            return df;
        }
        if (o instanceof Integer) {
            return (Integer) o;
        }
        try {
            if (o instanceof Byte) {
                return ((Byte) o).intValue();
            }
            if (o instanceof Short) {
                return ((Short) o).intValue();
            }
            if (o instanceof BigInteger) {
                return ((BigInteger) o).intValue();
            }
            if (o instanceof Double) {
                return ((Double) o).intValue();
            }
            if (o instanceof BigDecimal) {
                return ((BigDecimal) o).intValue();
            }
            return Integer.parseInt(o.toString());
        } catch (Exception e) {
            return df;
        }
    }

    public static long NL(Object o) {
        return NL(o, 0L);
    }

    public static long NL(Object o, long df) {
        if (o == null) {
            return df;
        }
        if (o instanceof Long) {
            return (Long) o;
        }
        if (o instanceof Integer) {
            return (Integer) o;
        }
        if (o instanceof Byte) {
            return (Byte) o;
        }
        if (o instanceof Short) {
            return (Short) o;
        }
        if (o instanceof BigInteger) {
            return ((BigInteger) o).longValue();
        }
        if (o instanceof BigDecimal) {
            return ((BigDecimal) o).longValue();
        }
        try {
            return Long.parseLong(o.toString());
        } catch (Exception e) {
            return df;
        }
    }

    public static Long NLorNull(Object o, Long df) {
        if (o == null) {
            return df;
        }
        if (o instanceof Long) {
            return (Long) o;
        }
        try {
            if (o instanceof Integer) {
                return ((Integer) o).longValue();
            }
            if (o instanceof Byte) {
                return ((Byte) o).longValue();
            }
            if (o instanceof Short) {
                return ((Short) o).longValue();
            }
            if (o instanceof BigInteger) {
                return ((BigInteger) o).longValue();
            }
            if (o instanceof BigDecimal) {
                return ((BigDecimal) o).longValue();
            }
            return Long.parseLong(o.toString());
        } catch (Exception e) {
            return df;
        }
    }

    public static String NS(Object o) {
        return NS(o, "");
    }

    public static String NS(Object o, String df) {
        try {
            return o.toString();
        } catch (Exception e) {
            return df;
        }
    }

    public static short NShort(Object o) {
        return NShort(o, (short) 0);
    }

    public static short NShort(Object o, short df) {
        if (o == null) {
            return df;
        }
        if (o instanceof Byte) {
            return (Byte) o;
        }
        if (o instanceof Short) {
            return (Short) o;
        }
        if (o instanceof BigInteger) {
            return ((BigInteger) o).shortValue();
        }
        if (o instanceof BigDecimal) {
            return ((BigDecimal) o).shortValue();
        }
        try {
            return Short.parseShort(o.toString());
        } catch (Exception e) {
            return df;
        }
    }

    public static boolean NB(Object o) {
        return NB(o, false);
    }

    public static boolean NB(Object o, boolean df) {
        if (o == null) {
            return df;
        }
        if (o instanceof Boolean) {
            return (Boolean) o;
        }
        if (o instanceof Short || o instanceof Long || o instanceof Byte || o instanceof Integer) {
            return (NI(o, 0) != 0);
        }
        if (o instanceof String) {
            String v = NS(o);
            return (v.equalsIgnoreCase("true") || v.equalsIgnoreCase("yes"));
        }
        try {
            return Boolean.parseBoolean(o.toString());
        } catch (Exception e) {
            return df;
        }
    }

    public static float NFloat(Object o) {
        return NFloat(o, 0F);
    }

    public static float NFloat(Object o, float df) {
        if (o == null) {
            return df;
        }
        if (o instanceof Float) {
            return (Float) o;
        }
        if (o instanceof Short || o instanceof Long || o instanceof Byte || o instanceof Integer) {
            return NI(o, 0);
        }
        try {
            return Float.parseFloat(o.toString());
        } catch (Exception e) {
            return df;
        }
    }

    public static double NDB(Object o) {
        return NDB(o, 0D);
    }

    public static double NDB(Object o, double df) {
        if (o == null) {
            return df;
        }
        if (o instanceof Double) {
            return (Double) o;
        }
        if (o instanceof Float) {
            return (Float) o;
        }
        if (o instanceof Short || o instanceof Long || o instanceof Byte || o instanceof Integer) {
            return NI(o, 0);
        }
        try {
            return Double.parseDouble(o.toString());
        } catch (Exception e) {
            return df;
        }
    }

    public static Double NDBorNull(Object o, Double df) {
        if (o == null) {
            return df;
        }
        if (o instanceof Double) {
            return (Double) o;
        }
        try {
            if (o instanceof Float) {
                return ((Float) o).doubleValue();
            }
            if (o instanceof Short || o instanceof Long || o instanceof Byte || o instanceof Integer) {
                return NIorNull(o, null).doubleValue();
            }
            return Double.parseDouble(o.toString());
        } catch (Exception e) {
            return df;
        }
    }

    public static BigDecimal NDec(Object o) {
        return NDec(o, BigDecimal.ZERO);
    }

    public static BigDecimal NDec(Object o, BigDecimal df) {
        if (o == null) {
            return df;
        }
        if (o instanceof BigDecimal) {
            return (BigDecimal) o;
        }
        try {
            if (o instanceof Long || o instanceof Integer || o instanceof Byte || o instanceof Short) {
                return BigDecimal.valueOf(NLorNull(o, null));
            }
            if (o instanceof Float || o instanceof Double) {
                return BigDecimal.valueOf(NDBorNull(o, null));
            }
            return BigDecimal.valueOf(NDBorNull(o, null));
        } catch (Exception e) {
            return df;
        }
    }

    public static Date NDT(Object o) {
        return NDT(o, new Date());
    }

    public static Date NDT(Object o, Date df) {
        if (o == null) {
            return df;
        }
        if (o instanceof Date) {
            return (Date) o;
        }
        if (o instanceof Long) {
            return new Date((Long) o);
        }
        if (o instanceof Integer) {
            String v = o.toString();
            if (v.length() == 8) {
                return new Date(Integer.parseInt(v.substring(0, 4)), Integer.parseInt(v.substring(5, 6)),
                        Integer.parseInt(v.substring(7, 8)));
            }
        }
        try {
            return DateUtils.parseDate(o.toString());
        } catch (Exception e) {
            return df;
        }
    }

    public static String DateTimeStr(Date date) {
        return FORMAT_DATE_TIME.format(date);
    }

    public static String TimeStr(Date date) {
        return FORMAT_TIME.format(date);
    }

    public static String DateStr(Date date) {
        return FORMAT_DATE.format(date);
    }

    public static void main(String[] args) {
        Object o1 = System.currentTimeMillis();
        Object o2 = "20160223";
        Object o3 = "2016/02/23T08:09:01";
        Object o4 = "2016-02-23 08:09:01";
        System.out.println(DateTimeStr(NDT(o1)));
        System.out.println(DateTimeStr(NDT(o2)));
        System.out.println(DateTimeStr(NDT(o3)));
        System.out.println(DateTimeStr(NDT(o4)));
    }

    /**
     * 下划线字符串转驼峰
     *
     * @param str
     * @return
     */
    public static String getHumpString(String str) {
        StringBuffer sb = new StringBuffer();
        Pattern p = Pattern.compile("_(\\w)");
        Matcher m = p.matcher(str);
        while (m.find()) {
            m.appendReplacement(sb, m.group(1).toUpperCase());
        }
        m.appendTail(sb);
        return sb.toString();
    }

    /**
     * 驼峰转下划线
     *
     * @param camelCaseName
     * @return
     */
    public static String underscoreName(String camelCaseName) {
        StringBuilder result = new StringBuilder();
        if (camelCaseName != null && camelCaseName.length() > 0) {
            result.append(camelCaseName.substring(0, 1).toLowerCase());
            for (int i = 1; i < camelCaseName.length(); i++) {
                char ch = camelCaseName.charAt(i);
                if (Character.isUpperCase(ch)) {
                    result.append("_");
                    result.append(Character.toLowerCase(ch));
                } else {
                    result.append(ch);
                }
            }
        }
        return result.toString();
    }

    /**
     * 获取汉字的unicode
     *
     * @param str
     * @return
     */
    public static String stringToUnicode(String str) {
        char[] chars = str.toCharArray();
        String returnStr = "";
        for (int i = 0; i < chars.length; i++) {
            returnStr += "u" + Integer.toString(chars[i], 16);
            //returnStr += "\\u" + Integer.toString(chars[i], 16);
        }
        return returnStr;
    }

    /**
     * list去重复
     *
     * @param list
     * @return
     */
    public static List removeDuplicate(List list) {
        HashSet hashSet = new HashSet(list);
        list.clear();
        list.addAll(hashSet);
        return list;
    }


    /**
     * 将Map中的key由下划线转换为驼峰
     *
     * @param map
     * @return
     */
    public static Map<String, Object> formatHumpName(Map<String, Object> map) {
        Map<String, Object> newMap = new HashMap<String, Object>();
        Iterator<Entry<String, Object>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, Object> entry = it.next();
            String key = entry.getKey();
            String newKey = toFormatCol(key);
            newMap.put(newKey, entry.getValue());
        }
        return newMap;
    }

    public static String toFormatCol(String colName) {
        StringBuilder sb = new StringBuilder();
        String[] str = colName.toLowerCase().split("_");
        int i = 0;
        for (String s : str) {
            if (s.length() == 1) {
                s = s.toUpperCase();
            }
            i++;
            if (i == 1) {
                sb.append(s);
                continue;
            }
            if (s.length() > 0) {
                sb.append(s.substring(0, 1).toUpperCase());
                sb.append(s.substring(1));
            }
        }
        return sb.toString();
    }

    /**
     * 将List中map的key值命名方式格式化为驼峰
     *
     * @param
     * @return
     */
    public static List<Map<String, Object>> formatHumpNameForList(List<Map<String, Object>> list) {
        List<Map<String, Object>> newList = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> o : list) {

            newList.add(formatHumpName(o));
        }
        return newList;
    }

}
