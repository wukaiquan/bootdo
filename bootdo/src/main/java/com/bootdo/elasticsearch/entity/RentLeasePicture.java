package com.bootdo.elasticsearch.entity;


import com.bootdo.elasticsearch.anno.EsCloumn;
import lombok.Data;

@Data
public class RentLeasePicture {

    @EsCloumn(name = "LEASE_PICTURE_ID")
    private Long leasePictureId;
    @EsCloumn(name = "PICTURE_URL")
    private String priceUrl;
    @EsCloumn(name = "TYPE")
    private String type;

}
