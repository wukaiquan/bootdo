package com.bootdo.web.service;

import com.bootdo.common.utils.JsonData;
import org.springframework.stereotype.Service;

/**
 * @Author wukq
 * @Date: 2020/4/13
 * @Description:
 */
@Service
class SendSMSServiceImpl implements SendSMSService {
    /**
     * 具体需要发送短信的接口，需要购买短信接口
     */
    public JsonData sendMsg(String receivePhone, String content) {
        System.out.println("------------------------发送短信成功,手机号:" + receivePhone + "。短信内容:" + content);
        return JsonData.returnJsonData(1, null, "操作成功", true);
    }
}
