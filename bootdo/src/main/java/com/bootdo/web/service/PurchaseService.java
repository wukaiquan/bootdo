package com.bootdo.web.service;

import com.bootdo.common.utils.JsonData;
import com.bootdo.entity.PurchaseApply;
import org.activiti.engine.runtime.ProcessInstance;

import java.util.Map;

/**
 * @Author wukq
 * @Date: 2020/7/20 18:22
 * @Description:
 */
public interface PurchaseService {
  /**
   * 开启工作流
   */
  ProcessInstance startWorkflow(PurchaseApply purchase, String userId, Map<String, Object> variables);

  /**
   * 根据采购id获取采购
   */
  PurchaseApply getPurchase(int purchaseId);

  /**
   * 更新purchase
   */
  JsonData updatePurchase(PurchaseApply purchase);
}
