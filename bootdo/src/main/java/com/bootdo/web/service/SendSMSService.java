package com.bootdo.web.service;

import com.bootdo.common.utils.JsonData;

/**
 * @Author wukq
 * @Date: 2020/4/13
 * @Description:
 */
public interface SendSMSService {

    JsonData sendMsg(String receivePhone, String content);
}
