package com.bootdo.web.dao;

import com.bootdo.entity.PurchaseApply;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PurchaseApplyMapper {
  /**
   * 保存采购单
   */
  int save(PurchaseApply purchase);

  /**
   * 查询采购单
   */
  PurchaseApply getPurchaseApply(int id);

  /**
   * 更新采购
   */
  int updatePurchaseByParams(PurchaseApply purchase);
}
