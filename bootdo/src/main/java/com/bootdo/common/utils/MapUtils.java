package com.bootdo.common.utils;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author TangWenChi
 * @date 2019/6/17 0017
 * @desc 使用reflect进行转换
 */
public class MapUtils {

    /**
     * 将mao转换为Object对象
     *
     * @param map
     * @param beanClass
     * @return
     */
    public static Object mapToObject(Map<String, Object> map, Class<?> beanClass) {
        try {
            if (map == null) {
                return null;
            }
            Object obj = beanClass.newInstance();
            Field[] fields = obj.getClass().getDeclaredFields();
            for (Field field : fields) {
                int mod = field.getModifiers();
                if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
                    continue;
                }
                field.setAccessible(true);
                field.set(obj, map.get(field.getName()));
            }
            return obj;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /***
     * 将具体的对象转换为Map对象
     * @param obj
     * @return
     */
    public static Map<String, Object> objectToMap(Object obj) {
        try {
            if (obj == null) {
                return null;
            }
            Map<String, Object> map = new HashMap();
            Field[] declaredFields = obj.getClass().getDeclaredFields();
            for (Field field : declaredFields) {
                field.setAccessible(true);
                map.put(field.getName(), field.get(obj));
            }
            return map;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    /**
     * bean包含文件 to map
     *
     * @param t
     * @param <T>
     * @return
     */
    public static <T> MultiValueMap beanToMultiMap(T t) throws Exception {
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        Class<?> clazz = t.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Object object = field.get(t);

            if (!StringUtils.isNullOrEmpty(object)) {
                // 文件转成bytearrayresource
                if (field.getName().equals("grossFileList")) {

                    List<MultipartFile> files = (List<MultipartFile>) object;
                    for (MultipartFile file : files) {
                        ByteArrayResource resource = new ByteArrayResource(file.getBytes()) {
                            @Override
                            public String getFilename() {
                                try {
                                    return URLEncoder.encode(file.getOriginalFilename(), "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    throw new RuntimeException(e.getMessage());
                                }
                            }
                        };
                        map.add("grossFileListByte", resource);
                    }
                } else if (field.getName().equals("tareFileList")) {
                    List<MultipartFile> files = (List<MultipartFile>) object;
                    for (MultipartFile file : files) {
                        ByteArrayResource resource = new ByteArrayResource(file.getBytes()) {
                            @Override
                            public String getFilename() {
                                try {
                                    return URLEncoder.encode(file.getOriginalFilename(), "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    throw new RuntimeException(e.getMessage());
                                }
                            }
                        };
                        map.add("tareFileListByte", resource);
                    }
                } else {
                    map.add(field.getName(), object);
                }
            } else {
                map.add(field.getName(), object);
            }


        }
        return map;
    }

}
