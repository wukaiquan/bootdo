package com.bootdo.common.utils;

import java.util.HashMap;
import java.util.Map;

public class JsonData {

  private String msg = "";   // 返回消息
  private int code = 0;    // 返回请求状态码， 0：请求失败， 1：请求成功
  private Object data = null; // 返回数据
  private boolean success = false;// 请求状态 false：请求失败 true：请求成功

  public static JsonData returnJsonData(int code, Object data, String msg, boolean success) {

    if (data == null) {
      data = new HashMap<String, Object>();
    }

    JsonData jsonData = new JsonData();
    jsonData.setCode(code);
    jsonData.setData(data);
    jsonData.setmsg(msg);
    jsonData.setSuccess(success);
    return jsonData;
  }

  public static JsonData returnJsonDataOK(Object data, String msg) {

    if (data == null) {
      data = new HashMap<String, Object>();
    }

    JsonData jsonData = new JsonData();
    jsonData.setCode(1);
    jsonData.setData(data);
    jsonData.setmsg(msg);
    jsonData.setSuccess(true);
    return jsonData;
  }

  public static JsonData returnJsonDataOK(Object data) {

    if (data == null) {
      data = new HashMap<String, Object>();
    }

    JsonData jsonData = new JsonData();
    jsonData.setCode(1);
    jsonData.setData(data);
    jsonData.setmsg("操作成功");
    jsonData.setSuccess(true);
    return jsonData;
  }

  public static JsonData returnJsonDataOK() {


    JsonData jsonData = new JsonData();
    jsonData.setCode(1);
    jsonData.setData(null);
    jsonData.setmsg("操作成功");
    jsonData.setSuccess(true);
    return jsonData;
  }

  public static JsonData returnJsonDataError(Object data, String msg) {

    if (data == null) {
      data = new HashMap<String, Object>();
    }

    JsonData jsonData = new JsonData();
    jsonData.setCode(0);
    jsonData.setData(data);
    jsonData.setmsg(msg);
    jsonData.setSuccess(false);
    return jsonData;
  }

  public static JsonData returnJsonDataError(Object data) {

    if (data == null) {
      data = new HashMap<String, Object>();
    }

    JsonData jsonData = new JsonData();
    jsonData.setCode(0);
    jsonData.setData(data);
    jsonData.setmsg("操作失败");
    jsonData.setSuccess(false);
    return jsonData;
  }

  public static JsonData returnJsonDataError(String msg) {

    JsonData jsonData = new JsonData();
    jsonData.setCode(0);
    jsonData.setData(null);
    jsonData.setmsg(msg);
    jsonData.setSuccess(false);
    return jsonData;
  }

  public static JsonData returnJsonDataError() {
    JsonData jsonData = new JsonData();
    jsonData.setCode(0);
    jsonData.setData(null);
    jsonData.setmsg("操作失败");
    jsonData.setSuccess(false);
    return jsonData;
  }

  public void setJson(String msg, Object data, int code, boolean success) {
    if (data == null) {
      data = new HashMap<String, Object>();
    }
    this.code = code;
    this.msg = msg;
    this.data = data;
    this.success = success;
  }

  public void setJson(String msg, int code, boolean success) {
    this.code = code;
    this.msg = msg;
    this.success = success;
  }

  public void setJson(int code, Map data, String msg, boolean success) {
    if (data == null) {
      data = new HashMap<String, Object>();
    }
    this.setCode(code);
    this.setData(data);
    this.setmsg(msg);
    this.setSuccess(success);
  }

  public String getmsg() {
    return msg;
  }

  public void setmsg(String msg) {
    this.msg = msg;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    if (data == null) {
      data = new HashMap<String, Object>();
    }
    this.data = data;
  }

  public boolean issuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

/*  @Override
  public String toString() {
    return "JsonData [msg=" + msg + ", code=" + code + ", data=" + data + ", success=" + success
        + "]";
  }*/

}
