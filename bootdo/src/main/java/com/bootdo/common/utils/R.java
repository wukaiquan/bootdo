package com.bootdo.common.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 得改， error(1, "操作失败"); 1是true,0是false
 */
public class R extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;

	private String msg = "";   // 返回消息
	private int code = 0;    // 返回请求状态码， 0：请求失败， 1：请求成功
	private Object data = null; // 返回数据
	private boolean success = false;// 请求状态 false：请求失败 true：请求成功


	public R() {
		put("code", 0);
		put("msg", "操作成功");
	}


	public R(int initialCapacity, String msg, int code, Object data, boolean success) {
		super(initialCapacity);
		this.msg = msg;
		this.code = code;
		this.data = data;
		this.success = success;
	}

	public R(int code, Object data, String msg, boolean success) {
		this.msg = msg;
		this.code = code;
		this.data = data;
		this.success = success;
	}

	public R(Map<? extends String, ?> m, String msg, int code, Object data, boolean success) {
		super(m);
		this.msg = msg;
		this.code = code;
		this.data = data;
		this.success = success;
	}

	public static R error() {
		return error(1, "操作失败");
	}

	public static R error(String msg) {
		return error(500, msg);
	}

	public static R error(int code, String msg) {
		R r = new R();
		r.put("code", code);
		r.put("msg", msg);
		return r;
	}

	public static R ok(String msg) {
		R r = new R();
		r.put("msg", msg);
		return r;
	}

	public static R ok(Map<String, Object> map) {
		R r = new R();
		r.putAll(map);
		return r;
	}

	public static R ok() {
		return new R();
	}

	public static R returnR(int code, Object data, String msg, boolean success) {

		if (data == null) {
			data = new HashMap<String, Object>();
		}
		return new R(code, data, msg, success);
	}

	public static R returnOK(Object data, String msg) {

		if (data == null) {
			data = new HashMap<String, Object>();
		}
		return new R(1, data, msg, true);
	}

	public static R returnOK(Object data) {

		if (data == null) {
			data = new HashMap<String, Object>();
		}
		return new R(1, data, "操作成功", true);
	}

	public static R returnError(Object data, String msg) {

		if (data == null) {
			data = new HashMap<String, Object>();
		}
		return new R(0, data, msg, false);
	}

	public static R returnError(Object data) {

		if (data == null) {
			data = new HashMap<String, Object>();
		}
		return new R(0, data, "操作失败", false);
	}

	public static R returnError(String msg) {
		return new R(0, null, msg, false);
	}

	@Override
	public R put(String key, Object value) {
		super.put(key, value);
		return this;
	}

	public void setJson(String msg, Object data, int code, boolean success) {
		if (data == null) {
			data = new HashMap<String, Object>();
		}
		this.code = code;
		this.msg = msg;
		this.data = data;
		this.success = success;
	}

	public void setJson(String msg, int code, boolean success) {
		this.code = code;
		this.msg = msg;
		this.success = success;
	}

	public void setJson(int code, Map data, String msg, boolean success) {
		if (data == null) {
			data = new HashMap<String, Object>();
		}
		this.setCode(code);
		this.setData(data);
		this.setmsg(msg);
		this.setSuccess(success);
	}

	public String getmsg() {
		return msg;
	}

	public void setmsg(String msg) {
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		if (data == null) {
			data = new HashMap<String, Object>();
		}
		this.data = data;
	}

	public boolean issuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
}
