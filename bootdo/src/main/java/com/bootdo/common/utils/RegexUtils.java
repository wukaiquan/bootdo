package com.bootdo.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hang_yu on 2018/6/26 0026.
 */
public class RegexUtils {

    /**
     * 汉字验证
     */
    public static final String CHINESE_REGEX = "^[\\u4e00-\\u9fa5]*$";

    /**
     * 温度验证
     */
    public static final String TEMPERATURE_REGEX = "^\\d{2}.\\d$";

    /**
     * 6到18位字母数字
     */
    public static final String USERNAME_PASSWORD_REGEX = "^[A-Za-z0-9]{6,18}$";

    /**
     * 1到20位字母数字,门禁卡号
     */
    public static final String CARD_NUMBER_REGEX = "^[A-Za-z0-9]{1,20}$";

    /**
     * 6到18位字母数字
     */
    public static final String CORP_USERNAME_REGEX = "^[A-Za-z][A-Za-z0-9_]{5,19}$";

    /**
     * 1到11位 纯数字
     */
    public static final String PHONE_NUMBER_REGEX = "^[0-9]{1,11}$";

    /**
     * 发票号8位纯数字
     */
    public static final String INVOICE_CODE_REGEX = "^[0-9]{8}$";

    /**
     * 11位数字
     */
    //public static final String PHONE_REGEX = "^\\d{11}$";
    public static final String PHONE_REGEX = "^[1]\\d{10}$"; //"[1]"代表第1位为数字1，"\\d{10}"代表后面是可以是0～9的数

    /**
     * 18位身份证号码验证
     */
    public static final String IDCARD_NUMBER_REGEX = "^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$";

    /**
     * 时间格式yyyy-MM-dd HH:mm:ss
     */
    public static final String DATETIME_REGEX = "^((19|20|21)[0-9]{2})-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01]) "
            + "([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$";

    /**
     * 时间格式HH:mm:ss
     */
    public static final String TIME_REGEX = "^([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$";

    /**
     * 时间格式yyyy-MM-dd
     */
    public static final String DATE_REGEX = "^((19|20|21)[0-9]{2})-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$";

    /**
     * 时间格式yyyy-MM
     */
    public static final String MONTH_REGEX = "^((19|20|21)[0-9]{2})-(0?[1-9]|1[012])$";

    /**
     * 数字或小数,不含字母
     */
    public static final String NUMBER_REGEX = "^[0-9]+([.]{1}[0-9]+){0,1}$";

    /**
     * 前8后2金额
     */
    public static final String MONEY_GEGEX = "(^[1-9](\\d{0,7}+)?(\\.\\d{1,2})?$)|(^0$)|(^\\d\\.\\d{1,2}$)";


    /**
     * 正负数字或小数,不含字母
     *
     * @decimal 小数位
     */
    public static final String POSITIVE_AND_NEGATIVE_NUMBER_REGEX = "^[\\-]{0,1}[0-9]+([.]{1}[0-9]{1,@decimal}){0,1}$";

    /**
     * 数字或小数，可传入需要验证的位数
     */
    public static final String INTEGER_DECIMAL_NUMBER_REGEX = "^\\d{0,@integer}(\\.\\d{0,@decimal})?$";

    /**
     * 正则表达式验证,返回true验证成功
     *
     * @param str       要验证的字段
     * @param regexType 正则表达式
     */
    public static boolean checkByRegex(String str, String regexType) {
        if (StringUtils.isNullOrEmpty(str)) {
            return false;
        }
//    Matcher m = Pattern.compile(regexType).matcher(str);
//    return m.find();
        return str.matches(regexType);
    }

    /**
     * 正则表达式验证--正整数,返回true验证成功
     *
     * @param number  要验证的字段
     * @param integer 整数位
     * @param decimal 小数位
     */
    public static boolean checkNumberRegex(String number, int integer, int decimal) {
        if (StringUtils.isNullOrEmpty(number)) {
            return false;
        }
        String numberRegex = INTEGER_DECIMAL_NUMBER_REGEX.replaceAll("@integer", integer + "");
        numberRegex = numberRegex.replaceAll("@decimal", decimal + "");
        return number.matches(numberRegex);
    }

    /**
     * 正则表达式验证--正负整数,返回true验证成功
     *
     * @param number
     * @param decimal
     * @return
     */
    public static boolean checkNumberRegex(String number, int decimal) {
        if (StringUtils.isNullOrEmpty(number)) {
            return false;
        }
        String numberRegex = POSITIVE_AND_NEGATIVE_NUMBER_REGEX;
        numberRegex = numberRegex.replaceAll("@decimal", decimal + "");
        return number.matches(numberRegex);
    }

    /**
     * 下划线字符串转驼峰
     *
     * @param str
     * @return
     */
    public static String getHumpString(String str) {
        StringBuffer sb = new StringBuffer();
        Pattern p = Pattern.compile("_(\\w)");
        Matcher m = p.matcher(str);
        while (m.find()) {
            m.appendReplacement(sb, m.group(1).toUpperCase());
        }
        m.appendTail(sb);
        return sb.toString();
    }

    /**
     * 将百分比率转换为数字，适合0到100,其他情况返回null
     *
     * @param rateStr
     * @return
     */
    public static Integer changeRateToNumber(String rateStr) {
        Integer number = null;
        Integer index = rateStr.indexOf("%");
        if (index <= 0) {
            return number;
        }
        try {
            number = Integer.valueOf(rateStr.substring(0, index));
            if (number < 0 || number > 100) {
                return null;
            }
            return number;
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) {
        boolean b = checkNumberRegex("123.1", 5, 2);
        System.out.println(b);
    }


}
