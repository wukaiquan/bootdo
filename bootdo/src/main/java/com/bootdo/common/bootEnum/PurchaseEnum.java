package com.bootdo.common.bootEnum;

/**
 * @Author wukq
 * @Date: 2020/7/30 15:42
 * @Description:
 */
public enum PurchaseEnum {

  Purchase_Auditi("purchaseAuditi", "采购经理审批"),
  Finance_Audit("financeaudit", "财务审批"),
  Update_Apply("updateapply", "调整申请");

  private String key;

  private String name;


  PurchaseEnum(String key, String name) {
    this.key = key;
    this.name = name;
  }

  public static String getValues(String key) {
    for (PurchaseEnum item : PurchaseEnum.values()) {
      if (item.getKey().equals(key)) {
        return item.getName();
      }
    }
    return null;
  }

  public String getKey() {
    return key;
  }

  public String getName() {
    return name;
  }
}
